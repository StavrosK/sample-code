angular.module('StavrosApp')

.filter('translate', ['MultiLanguage', function(MultiLanguage) {

    return function(input, category) {
    	
    	var translatedString = MultiLanguage.translate(input);

        return translatedString;
    }
}])


.directive('translate', ['$rootScope', 'MultiLanguage', function ($rootScope, MultiLanguage) {
    return {
    	restrict: 'A',
    	replace: false,
    	scope: {
    		translate: '@'
    	},
        template: '{{translatedString}}',
        controller: ['$scope', 'MultiLanguage', function($scope, MultiLanguage){

        	$scope.translateString = function(){

        		$scope.translatedString = MultiLanguage.translate($scope.translate);
        	};

        	$scope.translateString();

        	$rootScope.$on('language-changed', function(){

        		$scope.translateString();
        	});
        }]
    };
}]);